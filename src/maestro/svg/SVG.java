package maestro.svg;

import android.graphics.*;
import android.graphics.drawable.Drawable;

import java.lang.ref.WeakReference;

public class SVG extends Drawable {

    public static final String TAG = SVG.class.getSimpleName();

    public static final String ROTATION = "Rotation";
    public static final String MUTATE_SCALE = "MutateScale";

    public static final float SCALE_FIT = Long.MAX_VALUE;

    private Picture picture;
    private RectF bounds;
    private RectF limits = null;
    private ColorFilter mColorFilter;
    private Paint mPaint = new Paint();
    private WeakReference<Bitmap> mBuffer;
    private WeakReference<Bitmap> mBitmap;
    private float mutateScale = 1f;
    private float mScale = 1f;
    private int alpha = 255;

    private int rotateAngle = 0;

    {
        mPaint.setAntiAlias(true);
        mPaint.setFilterBitmap(true);
    }

    SVG(Picture picture, RectF bounds) {
        this.picture = picture;
        this.bounds = bounds;
    }

    public SVG setMutateScale(float mutateScale) {
        this.mutateScale = mutateScale;
        invalidateSelf();
        return this;
    }

    @Override
    public int getIntrinsicWidth() {
        return picture != null ? Math.round(picture.getWidth() * mScale) : -1;
    }

    @Override
    public int getIntrinsicHeight() {
        return picture != null ? Math.round(picture.getHeight() * mScale) : -1;
    }

    void setLimits(RectF limits) {
        this.limits = limits;
    }

    public Picture getPicture() {
        return picture;
    }

    public RectF getLocalBounds() {
        return bounds;
    }

    public RectF getLocalLimits() {
        return limits;
    }

    public void setRotation(int angle) {
        rotateAngle = angle;
        invalidateSelf();
    }

    public int getRotation() {
        return rotateAngle;
    }

    public SVG setScale(float scale) {
        mScale = scale;
        return this;
    }

    public Bitmap getBitmap(){
        mBitmap = new WeakReference<Bitmap>(Bitmap.createBitmap(getIntrinsicWidth(), getIntrinsicHeight(), Bitmap.Config.ARGB_8888));
        draw(new Canvas(mBitmap.get()));
        return mBitmap.get();
    }

    @Override
    public void draw(Canvas rootCanvas) {
        if (picture != null) {
            Canvas canvas = rootCanvas;
            final Rect bounds = getBounds();
            final int width = bounds.width();
            final int height = bounds.height();
            Bitmap bitmap = null;
            if (rootCanvas.isHardwareAccelerated()) {
                if (mBuffer == null || mBuffer.get() == null || (mBuffer.get().getWidth() != width || mBuffer.get().getHeight() != height)) {
                    recycle();
                    bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                    mBuffer = new WeakReference<Bitmap>(bitmap);
                } else {
                    bitmap = mBuffer.get();
                }
                canvas = new Canvas(bitmap);
            }
            canvas.save();
            float difX = width - width * mutateScale;
            float difY = height - height * mutateScale;
            if (difX != 0 || difY != 0) {
                difX /= 2;
                difY /= 2;
            }
            float scale = mScale == SCALE_FIT ? bounds.width() < bounds.height() ? (float) width / getOriginWidth() : (float) height / getOriginHeight() : mScale;
            canvas.rotate(rotateAngle, bounds.width() / 2, bounds.height() / 2);
            if (bitmap == null) {
                canvas.translate(bounds.left + difX, bounds.top + difY);
            }
            canvas.scale(scale * mutateScale, scale * mutateScale);
            canvas.drawPicture(picture);
            if (bitmap != null) {
                canvas.translate(bounds.left + difX, bounds.top + difY);
                mPaint.setAlpha(alpha);
                rootCanvas.drawBitmap(bitmap, 0, 0, mPaint);
            }
            canvas.restore();
        }
    }

    @Override
    public void setAlpha(int alpha) {
        this.alpha = alpha;
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        mColorFilter = cf;
    }

    @Override
    public int getOpacity() {
        return PixelFormat.TRANSLUCENT;
    }

    public int getOriginWidth() {
        return picture.getWidth();
    }

    public int getOriginHeight() {
        return picture.getHeight();
    }

    final void recycle() {
        if (mBuffer != null && mBuffer.get() != null) {
            mBuffer.get().recycle();
            mBuffer.clear();
            mBuffer = null;
        }
    }

}
