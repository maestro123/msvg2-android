package maestro.svg;

import android.graphics.*;
import android.view.Gravity;

/**
 * Created by artyom on 11/17/14.
 */
public class SVGMenuItem extends SVG {

    private Paint mRoundPaint = new Paint();
    private Paint mTextPaint = new Paint();
    private int height;
    private int radius;
    private int color;
    private int textColor;
    private Rect margins = new Rect();
    private String text;
    private int gravity = Gravity.RIGHT;
    private int maxChars = 3;

    {
        mRoundPaint.setAntiAlias(true);
        mTextPaint.setAntiAlias(true);
    }

    public SVGMenuItem(Picture picture, RectF bounds) {
        super(picture, bounds);
    }

    public SVGMenuItem setRadius(int radius) {
        this.radius = radius;
        return this;
    }

    public SVGMenuItem setHeight(int height) {
        this.height = height;
        return this;
    }

    public SVGMenuItem setColor(int color) {
        this.color = color;
        mRoundPaint.setColor(color);
        return this;
    }

    public SVGMenuItem setTextColor(int color) {
        this.textColor = color;
        mTextPaint.setColor(color);
        return this;
    }

    public SVGMenuItem setTextSize(float textSize) {
        mTextPaint.setTextSize(textSize);
        return this;
    }

    public SVGMenuItem setTextTypeface(Typeface typeface) {
        mTextPaint.setTypeface(typeface);
        return this;
    }

    public SVGMenuItem setMaxChars(int maxChars) {
        this.maxChars = maxChars;
        return this;
    }

    public SVGMenuItem setMargins(int left, int top, int right, int bottom) {
        margins.set(left, top, right, bottom);
        return this;
    }

    public SVGMenuItem setGravity(int gravity) {
        this.gravity = gravity;
        return this;
    }

    public SVGMenuItem setText(String text) {
        if (text != null && text.length() > maxChars) {
            this.text = text.substring(0, maxChars);
        } else
            this.text = text;
        invalidateSelf();
        return this;
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (text != null) {
            final float textWidth = mTextPaint.measureText(text, 0, text.length());
            final RectF bulletSize = new RectF(0, 0, Math.max(textWidth + radius, height), height);
            final Rect bounds = getBounds();
            final float startX = gravity == Gravity.RIGHT ? bounds.right - bounds.left - margins.right - bulletSize.width()
                    : gravity == Gravity.CENTER ? bounds.width() / 2 - bulletSize.width() / 2 : 0;
            final float startY = bounds.top - margins.height();
            canvas.save();
            canvas.translate(startX, startY);
            canvas.drawRoundRect(bulletSize, radius, radius, mRoundPaint);
            canvas.restore();
            canvas.drawText(text, startX + bulletSize.width() / 2 - textWidth / 2, startY + getTextHeight(mTextPaint) / 2 + bulletSize.height() / 2, mTextPaint);
        }
    }

    final float getTextHeight(Paint paint) {
        Rect rect = new Rect();
        paint.getTextBounds(new char[]{'A'}, 0, 1, rect);
        return rect.height();
    }

}
