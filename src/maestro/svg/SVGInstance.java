package maestro.svg;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.widget.ImageView;

import java.util.HashMap;

/**
 * Created by Artyom on 9/5/2015.
 */
public class SVGInstance {

    public static float DPI;

    private static Resources resources;

    public static void initialize(Context context) {
        resources = context.getResources();
        DPI = resources.getDisplayMetrics().density;
    }

    private static HashMap<String, SVG> svgCache = new HashMap<String, SVG>();
    private static HashMap<String, SVG> svgMenuCache = new HashMap<String, SVG>();

    public static void applySVG(ImageView imageView, int id, int color) {
        String key = new StringBuilder().append(id).append(color).toString();
        SVG svg = svgCache.get(id + color);
        if (svg == null) {
            svg = getDrawable(resources, id, color);
            svgCache.put(key, svg);
        }
        imageView.setImageDrawable(svg);
    }

    public static void applySVG(ImageView imageView, int id) {
        String key = String.valueOf(id);
        SVG svg = svgCache.get(id);
        if (svg == null) {
            svg = getDrawable(resources, id);
            svgCache.put(key, svg);
        }
        imageView.setImageDrawable(svg);
    }

    public static void applySVG(ImageView imageView, int id, float scale) {
        String key = new StringBuilder().append(id).append(scale).toString();
        SVG svg = svgCache.get(id);
        if (svg == null) {
            svg = getDrawable(resources, id, scale);
            svgCache.put(key, svg);
        }
        imageView.setImageDrawable(svg);
    }

    public static void applySVG(ImageView imageView, int id, int color, float scale) {
        String key = new StringBuilder().append(id).append(color).append(scale).toString();
        SVG svg = svgCache.get(key);
        if (svg == null) {
            svg = getDrawable(resources, id, Color.BLACK, color, scale);
            svgCache.put(key, svg);
        }
        imageView.setImageDrawable(svg);
    }

    public static SVGMenuItem getMenuDrawable(int id, int color) {
        String key = new StringBuilder().append(id).append(color).toString();
        SVG svg = svgMenuCache.get(key);
        if (svg == null) {
            svg = getMenuDrawable(resources, id, color);
            svgMenuCache.put(key, svg);
        }
        return (SVGMenuItem) svg;
    }

    public static SVG getDrawable(int id, int color) {
        String key = new StringBuilder().append(id).append(color).toString();
        SVG svg = svgCache.get(key);
        if (svg == null) {
            svg = getDrawable(resources, id, color);
            svgCache.put(key, svg);
        }
        return svg;
    }

    public static SVG getDrawable(int id, int searchColor, int color) {
        String key = new StringBuilder().append(id).append(searchColor).append(color).toString();
        SVG svg = svgCache.get(key);
        if (svg == null) {
            svg = getDrawable(resources, id, searchColor, color);
            svgCache.put(key, svg);
        }
        return svg;
    }

    public static SVG getDrawable(int id, int replaceColor, float scale) {
        String key = new StringBuilder().append(id).append(replaceColor).append(scale).toString();
        SVG svg = svgCache.get(key);
        if (svg == null) {
            svg = getDrawable(resources, id, Color.BLACK, replaceColor, scale);
            svgCache.put(key, svg);
        }
        return svg;
    }

    public static SVG getDrawable(int id, float scale) {
        String key = new StringBuilder().append(id).append(scale).toString();
        SVG svg = svgCache.get(key);
        if (svg == null) {
            svg = getDrawable(resources, id, scale);
            svgCache.put(key, svg);
        }
        return svg;
    }

    public static SVG getDrawable(int id) {
        String key = String.valueOf(id);
        SVG svg = svgCache.get(key);
        if (svg == null) {
            svg = getDrawable(resources, id);
            svgCache.put(key, svg);
        }
        return svg;
    }

    public static SVG getDrawable(int id, int searchColor, int replaceColor, float scale) {
        String key = new StringBuilder().append(String.valueOf(id + searchColor + replaceColor)).append("sc=").append(scale).toString();
        SVG svg = svgCache.get(key);
        if (svg == null) {
            svg = getDrawable(resources, id, searchColor, replaceColor, scale);
            svgCache.put(key, svg);
        }
        return svg;
    }

    public static SVGMenuItem getMenuDrawable(Resources resources, int resourceId, int replaceColor) {
        SVG svg = getDrawable(resources, resourceId, Color.BLACK, replaceColor, 1f);
        SVGMenuItem item = new SVGMenuItem(svg.getPicture(), svg.getLocalBounds());
        item.setScale(DPI);
        return item;
    }

    public static SVG getDrawable(Resources resources, int resourceId) {
        return SVGParser.getSVGFromResource(resources, resourceId).setScale(DPI);
    }

    public static SVG getDrawable(Resources resources, int resourceId, int searchColor, int replaceColor) {
        return SVGParser.getSVGFromResource(resources, resourceId, searchColor, replaceColor).setScale(DPI);
    }

    public static SVG getDrawable(Resources resources, int resourceId, float scale) {
        return SVGParser.getSVGFromResource(resources, resourceId).setScale(scale);
    }

    public static SVG getDrawable(Resources resources, int resourceId, int replaceColor) {
        return getDrawable(resources, resourceId, Color.BLACK, replaceColor);
    }

    public static SVG getDrawable(Resources resources, int resourceId, int searchColor, int replaceColor, float scale) {
        return SVGParser.getSVGFromResource(resources, resourceId, searchColor, replaceColor).setScale(scale);
    }

}
